package SacADos.java;


public class item {
	private float poids;
	private float valeur;
	private String nom;
	private boolean estDedans=false;
	
	public float getPoids() {
		return poids;
	}
	public float getValeur() {
		return valeur;
	}
	public String getNom() {
		return nom;
	}
	public boolean getEstDedans() {
		return estDedans;
	}
	public boolean setEstDedans() {
		estDedans=true;
		return estDedans;
	}
	
	public item(String nom, float poids, float valeur) {
		this.nom=nom;
		this.poids=poids;
		this.valeur=valeur;
	}
	@Override
	public String toString() {
		return "item [poids=" + poids + ", valeur=" + valeur + ", nom=" + nom + "]" + " rapport " + (this.valeur/this.poids);
	}

}
