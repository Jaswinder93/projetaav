package SacADos.java;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

public class SacAdos {
	private String chemin;
	private float poids_maximal;
	private LinkedList<item> contenuSac = new LinkedList<item>();
	private LinkedList<item> listObjet = new LinkedList<item>();
	private ArrayList<Boolean> verif =  new ArrayList<Boolean>();
	
	
	public SacAdos() {
		
	}
	
	public SacAdos(String chemin,float poids_maximal) {
		this.chemin="C:\\Users\\jas93\\OneDrive\\Bureau\\2A_IUT\\Projet Java\\src\\item.txt";
		this.poids_maximal=poids_maximal;
		
	}

	public String getChemin() {
		return chemin;
	} 

	public float getPoids_maximal() {
		return poids_maximal;
	}
	
	public void Initialiser() {
		try{
			InputStream flux=new FileInputStream(chemin); 
			InputStreamReader lecture=new InputStreamReader(flux);
			BufferedReader buff=new BufferedReader(lecture);
			String ligne;
			String tab[];
			while ((ligne=buff.readLine())!=null){
				tab = ligne.split(" ; ");
				float poids = Float.parseFloat(tab[1]);
				float valeur = Float.parseFloat(tab[2]);
				item i = new item(tab[0],poids,valeur);
				listObjet.add(i);
			
			}
			//listObjet.forEach(System.out::println);
			buff.close(); 
			}		
			catch (Exception e){
			e.printStackTrace();
			}

	}
	
	public void ResoudreGlouton() {
		
		Collections.sort(listObjet, new Comparator<item>() {
			@Override
			public int compare(item o1,item o2) {
				float p1 =o1.getPoids();
				float p2 =o2.getPoids();
				float v1 =o1.getValeur();
				float v2 =o2.getValeur();
				float res1 = v1/p1;
				float res2=v2/p2;
				return Float.compare(res2,res1);
			}
		});
		
		listObjet.forEach(System.out::println);
		
		System.out.println("---------------------------------");
		System.out.println("---------------------------------");
		

		float poidsActuel = 0;
		for(item i : listObjet) {
				boolean etat;
				poidsActuel+=i.getPoids();
				if(poidsActuel<=poids_maximal) {
					etat=i.getEstDedans();
					if(etat==false) {
						contenuSac.add(i);
						verif.add(true);
					}
					i.setEstDedans();
		}
		}
		System.out.println(poids_maximal);
		verif.forEach(System.out::println);
		contenuSac.forEach(System.out::println);
	}
		
}
	
	


